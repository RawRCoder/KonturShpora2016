﻿using System;
using System.Net;
using System.Threading.Tasks;
using log4net;
using System.Threading;

namespace ClusterServer
{
    public static class HttpListenerExtensions
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(HttpListenerExtensions));
        private static readonly int MaxAccepts = 4*Environment.ProcessorCount;


        public static void StartProcessingRequests(this HttpListener listener, Action<HttpListenerContext> callback)
        {
            listener.Start();
            Console.WriteLine("Server started listening prefixes: {0}", string.Join(";", listener.Prefixes));
            listener.HttpServerLoop(callback);
        }

        private static async void HttpServerLoop(this HttpListener listener, Action<HttpListenerContext> callback)
        {
            var sem = new Semaphore(MaxAccepts, MaxAccepts);

            while (true)
            {
                sem.WaitOne();

#pragma warning disable 4014
                listener.GetContextAsync().ContinueWith(async (t) =>
                {
                    try
                    {
                        sem.Release();

                        var context = await t;
                        context.ProcessRequest(callback);
                        return;
                    }
                    catch (Exception e)
                    {
                        Log.Error(e);
                    }
                });
#pragma warning restore 4014
            }
            /*
            while (true)
            {
                try
                {
                    var context = await listener.GetContextAsync();
                    Task.Run(() => context.ProcessRequest(callback));
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }*/
        }

        private static void ProcessRequest(this HttpListenerContext context, Action<HttpListenerContext> callback)
        {
            try
            {
                callback(context);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            finally
            {
                context.Response.Close();
            }
        }

    }
}