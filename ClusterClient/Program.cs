﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ClusterClient.Clients;
using Fclp;
using log4net;
using log4net.Config;

namespace ClusterClient
{
    class Program
    {
        static IEnumerable<ClusterClientBase> GetClients(string[] addresses)
        {
            var baseType = typeof (ClusterClientBase);
            var types = Assembly.GetCallingAssembly()
                .GetTypes()
                .Where(t => baseType.IsAssignableFrom(t));
            return from type in types
                select type.GetConstructor(new[] {typeof (string[])}) into ctor
                where ctor != null
                select ctor.Invoke(new object[] {addresses}) as ClusterClientBase;
        }
        
        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            string[] replicaAddresses;
            if (!TryGetReplicaAddresses(args, out replicaAddresses))
                return;

            try
            {
                var clients = GetClients(replicaAddresses).ToArray();
                var queries = Enumerable.Range(0, MaxConcurrentRequests*2).Select(i => i.ToString()).ToArray();

                foreach (var client in clients)
                {
                    TestClient(client, queries);
                }
            }
            catch (Exception e)
            {
                Log.Fatal(e);
            }
        }

        private static TimeSpan Timeout = TimeSpan.FromSeconds(15);

        private static int MaxConcurrentRequests = Environment.ProcessorCount*4;
        private static void TestClient(ClusterClientBase client, string[] queries)
        {
            Console.WriteLine($"Testing {client.GetType()} started");
            var timer = Stopwatch.StartNew();
            var sem = new Semaphore(MaxConcurrentRequests, MaxConcurrentRequests);
            Task.WaitAll(queries.Select(
                async query =>
                {
                    sem.WaitOne();
                    var ok = true;
                    try
                    {
                        await client.RequestAsync(query, Timeout);
                    }
                    catch (TimeoutException)
                    {
                        ok = false;
                    }
                    sem.Release();
                    var time = timer.ElapsedMilliseconds;
                    Console.WriteLine(ok
                            ? $"[*] {time,4} ms ({Timeout.TotalMilliseconds - time,4:N0} ms left) : Processed query \"{query}\""
                            : $"[ ] {time,4} ms ({Timeout.TotalMilliseconds - time,4:N0} ms left) : Timeout on query \"{query}\"");
                }).ToArray());
            Console.WriteLine($"Testing {client.GetType()} finished");
            foreach (var stats in client.GetDebugRatingInfo(Timeout).OrderByDescending(s => s.KnownRating))
            {
                Console.WriteLine($"\t{stats}");
            }
        }

        private static bool TryGetReplicaAddresses(string[] args, out string[] replicaAddresses)
        {
            var argumentsParser = new FluentCommandLineParser();
            string[] result = {};

            argumentsParser.Setup<string>('f', "file")
                .WithDescription("Path to the file with replica addresses")
                .Callback(fileName => result = File.ReadAllLines(fileName))
                .Required();

            argumentsParser.SetupHelp("?", "h", "help")
                .Callback(text => Console.WriteLine(text));

            var parsingResult = argumentsParser.Parse(args);

            if (parsingResult.HasErrors)
            {
                argumentsParser.HelpOption.ShowHelp(argumentsParser.Options);
                replicaAddresses = null;
                return false;
            }

            replicaAddresses = result;
            return !parsingResult.HasErrors;
        }

        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
    }
}
