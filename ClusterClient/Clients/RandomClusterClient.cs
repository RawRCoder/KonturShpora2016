﻿using System;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public class RandomClusterClient : ClusterClientBase
    {
        private readonly Random random = new Random();
        protected override bool NeedsRatingSort => false;

        public RandomClusterClient(string[] replicaAddresses)
            : base(replicaAddresses)
        {
        }

        protected override async Task<string> ProcessRequestAsync(string query, TimeSpan timeout)
        {
            string uri;
            lock (ReplicaAddresses)
                uri = ReplicaAddresses[random.Next(ReplicaAddresses.Length)];
            var task = MakeRequestToAsync(uri, query);
            await Task.WhenAny(task, Task.Delay(timeout));
            if (!task.IsCompleted)
                throw new TimeoutException();

            return task.Result;
        }

        protected override ILog Log => LogManager.GetLogger(typeof(RandomClusterClient));
    }
}