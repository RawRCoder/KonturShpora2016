﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClusterClient.Clients
{
    public class ServerStats
    {
        public string Uri { get; }
        private List<RequestStats> History { get; set; } = new List<RequestStats>();
        public static TimeSpan HistoryRange = TimeSpan.FromMinutes(5);
        public ServerStats(string uri)
        {
            Uri = uri;
        }
        public double KnownRating { get; private set; } = 1.0;

        public void Prepare(TimeSpan totalTime)
        {
            var now = DateTime.Now;
            lock (History)
            {
                History = History.SkipWhile(record => (now - record.RequestTime) > HistoryRange).ToList();
                KnownRating = GetRating(totalTime);
            }
        }
        public void Register(RequestStats stat)
        {
            lock (History) History.Add(stat);
        }

        private double GetRating(TimeSpan totalTime)
        {
            var totalMs = totalTime.TotalMilliseconds;
            if (!History.Any())
                return 1;
            return History.Sum(r => (totalTime - r.Latency).TotalMilliseconds)/(totalMs*History.Count);
        }

        public override string ToString()
            => $"{Uri} - rating {KnownRating}";
    }

    public class RequestStats
    {
        public bool Finished { get; set; } = false;
        public DateTime RequestTime { get; }
        public DateTime ResponseTime { get; set; }
        public TimeSpan Latency => (Finished ? ResponseTime : DateTime.Now) - RequestTime;
        
        public RequestStats(DateTime reqTime)
        {
            RequestTime = reqTime;
        }
        public RequestStats()
        {
            RequestTime = DateTime.Now;
        }

        public override string ToString()
            => !Finished ? $"{RequestTime} - ..." : $"{RequestTime} - {ResponseTime} : {Latency.TotalMilliseconds:N2} ms";
    }
}
