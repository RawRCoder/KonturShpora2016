﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public abstract class ClusterClientBase
    {
        private Dictionary<string, ServerStats> Servers { get; }
        protected string[] ReplicaAddresses { get; private set; }
        protected virtual bool NeedsRatingSort => true;
        protected TimeSpan SortingPeriod { get; set; } = TimeSpan.FromSeconds(0.5); // better ~ f(records count in servers' history); More records -> greater period, ...

        private DateTime LastSortingTime { get; set; } = DateTime.Now;
        private bool ShouldSort => NeedsRatingSort && (DateTime.Now - LastSortingTime) < SortingPeriod;

        protected ClusterClientBase(string[] replicaAddresses)
        {
            Servers = new Dictionary<string, ServerStats>(replicaAddresses.Length);
            foreach (var serverStats in replicaAddresses.Select(uri => new ServerStats(uri)))
                Servers.Add(serverStats.Uri, serverStats);
            ReplicaAddresses = Servers.Values.Select(ss => ss.Uri).ToArray();
        }
        
        private bool DoSorting(TimeSpan timeout, bool ignoreSortingDelay = false)
        {
            if (!ignoreSortingDelay && !ShouldSort)
                return false;
            foreach (var serverStats in Servers.Values)
                serverStats.Prepare(TimeSpan.FromTicks(timeout.Ticks / Servers.Count));
            LastSortingTime = DateTime.Now;
            ReplicaAddresses = Servers.Values.OrderByDescending(s => s.KnownRating).Select(s => s.Uri).ToArray();
            return true;
        }

        internal IEnumerable<ServerStats> GetDebugRatingInfo(TimeSpan timeout)
        {
            DoSorting(timeout, true);
            return Servers.Values;
        }

        public Task<string> RequestAsync(string query, TimeSpan timeout)
        {
            //Console.WriteLine($" === requesting {query} ...");
            DoSorting(timeout);
            return ProcessRequestAsync(query, timeout);
        }
        protected abstract Task<string> ProcessRequestAsync(string query, TimeSpan timeout);
        protected abstract ILog Log { get; }

        protected static HttpWebRequest CreateRequest(string uriStr)
        {
            var request = WebRequest.CreateHttp(Uri.EscapeUriString(uriStr));
            request.Proxy = null;
            request.KeepAlive = true;
            request.ServicePoint.UseNagleAlgorithm = false;
            request.ServicePoint.ConnectionLimit = 100500;
            return request;
        }

        protected async Task<string> ProcessRequestInternalAsync(WebRequest request)
        {
            var timer = Stopwatch.StartNew();
            using (var response = await request.GetResponseAsync())
            {
                var result = await new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEndAsync();
                Log.InfoFormat("Response from {0} received in {1} ms", request.RequestUri, timer.ElapsedMilliseconds);
                return result;
            }
        }

        protected async Task<string> MakeRequestToAsync(string uri, string query)
        {
            ServerStats server;
            var stat = new RequestStats();
            if (!Servers.TryGetValue(uri, out server))
                throw new ArgumentException($"Unknown server: {uri}", nameof(uri));
            server.Register(stat);
            var webRequest = CreateRequest($"{uri}?query={query}");
            Log.InfoFormat("Processing {0}", webRequest.RequestUri);

            var result = await ProcessRequestInternalAsync(webRequest);
            stat.ResponseTime = DateTime.Now;
            stat.Finished = true;
            return result;
        }
    }
}