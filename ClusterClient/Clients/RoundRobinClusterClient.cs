﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public class RoundRobinClusterClient : ClusterClientBase
    {
        protected override ILog Log => LogManager.GetLogger(typeof(RandomClusterClient));

        public RoundRobinClusterClient(string[] replicaAddresses) : base(replicaAddresses)
        {
        }
        protected override async Task<string> ProcessRequestAsync(string query, TimeSpan timeout)
        {
            var nTimeout = new TimeSpan(timeout.Ticks/ReplicaAddresses.Length);
            foreach (var uri in ReplicaAddresses)
            {
                var requestTask = MakeRequestToAsync(uri, query);
                await Task.WhenAny(Task.Delay(nTimeout), requestTask);
                if (requestTask.IsCompleted)
                    return requestTask.Result;
            }
            throw new TimeoutException();
        }
    }
}