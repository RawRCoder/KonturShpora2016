﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public class SmartClusterClient : ClusterClientBase
    {
        public SmartClusterClient(string[] replicaAddresses) : base(replicaAddresses)
        {
        }

        protected override ILog Log => LogManager.GetLogger(typeof(RandomClusterClient));

        protected override async Task<string> ProcessRequestAsync(string query, TimeSpan timeout)
        {
            var tasks = new List<Task<string>>();
            var nTimeout = new TimeSpan(timeout.Ticks / ReplicaAddresses.Length);
            foreach (var requestTask in ReplicaAddresses.Select(uri => MakeRequestToAsync(uri, query)))
            {
                tasks.Add(requestTask);
                var delayTask = Task.Delay(nTimeout);
                await Task.WhenAny(tasks.Concat(new[] {delayTask}));

                var finishedTask = tasks.FirstOrDefault(strTask => strTask.IsCompleted);
                if (finishedTask != null)
                    return finishedTask.Result;
            }
            throw new TimeoutException();
        }
    }
}