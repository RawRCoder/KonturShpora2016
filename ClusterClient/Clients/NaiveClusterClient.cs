﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;

namespace ClusterClient.Clients
{
    public class NaiveClusterClient : ClusterClientBase
    {
        protected override bool NeedsRatingSort => false;
        protected override ILog Log => LogManager.GetLogger(typeof(RandomClusterClient));

        public NaiveClusterClient(string[] replicaAddresses) : base(replicaAddresses)
        {
        }
        protected override async Task<string> ProcessRequestAsync(string query, TimeSpan timeout)
        {
            var tasks = new List<Task> {Task.Delay(timeout)};
            tasks.AddRange(ReplicaAddresses.Select(uri => MakeRequestToAsync(uri, query)));

            await Task.WhenAny(tasks.ToArray());
            var task = tasks.Skip(1).Cast<Task<string>>().FirstOrDefault(t => t.IsCompleted);
            if (task == null)
                throw new TimeoutException();
            return task.Result;
        }
    }
}